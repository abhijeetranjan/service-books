import { isJson, parseParams } from '../helpers/helper';

describe('testing helper functions', () => {
  test('it should return empty', () => {
    expect(isJson('')).toBe('');
  });

  test('it should return null', () => {
    expect(isJson(null)).toBe(null);
  });

  test('it should return object when input is valid', () => {
    const param = {
      success: 200,
      data: {
        value: 10
      }
    };
    const response = isJson(JSON.stringify(param));
    expect(response?.data?.value).toEqual(10);
  });

  test('it should return empty when sendParam is not specified', () => {
    const input = [
      {
        data_from: 'body'
      }
    ];

    const response = parseParams(null, input, null);
    expect(response).toStrictEqual({});
  });

});
