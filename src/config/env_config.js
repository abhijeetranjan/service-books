module.exports = {
  mongo_url: process.env.MONGO_URL,
  port: process.env.PORT,
  host: process.env.HOST,
  app_env: process.env.APP_ENV
};
