import mongoose from 'mongoose';
import constants from './constants';

// Remove the warning with Promise
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

const connectDb = () =>
  mongoose.connect(constants.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

export default connectDb;