import * as dotenv from 'dotenv';

dotenv.config();

const defaultConfig = {
  PORT: process.env.PORT,
  MONGO_URL: process.env.MONGO_URL,
  FRONTEND_URL: process.env.FRONTEND_URL
};

export default {
  ...defaultConfig
};
