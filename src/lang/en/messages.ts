export abstract class Messages {
  static readonly server = {
    connection_failure: 'Failed to connect to server',
    connection_success: 'Connected to server'
  }

  static readonly mongo = {
    connection_failure: 'Failed to connect mongo server',
    connection_success: 'Connected to database'
  };

  static readonly resource_not_found = {
    type: 'RESOURCE_NOT_FOUND',
    message: 'Resource does not exist or has been removed.'
  };

  static readonly something_went_wrong = {
    type: 'SOMETHING_WENT_WRONG',
    message: 'Something went wrong. Please Try Again'
  };

  static readonly field: {
    invalid: 'Field(s) is invalid'
  };
}