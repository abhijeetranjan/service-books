
import BookData from '../models/book.model';
import Helpers from '../helpers/Helpers';
import { Messages } from '../../../lang/en/messages';

class BookController {

  /**
   * save()
   * Method is used to create a new book
   * @author Abhijeet
   * @param req 
   * @param res 
   * @param next 
   */
  public async save(req, res, next) {
    try {

      if (!req.body) {
        const rdata = { req, res, status: 400, errors: Messages.field.invalid };
        return Helpers.transformResponse(rdata);
      }

      const { author, title, isbn, release_date } = req.body;
      let data = {
        author,
        title,
        isbn,
        release_date
      };
      const result = await BookData.model.create(data);
      const rdata = { req, res, status: 200, data: result };
      return Helpers.transformResponse(rdata);

    } catch (e) {
      next(e);
    }
  }

  /**
   * getBook()
   * Method is used to get specific book
   * @author Abhijeet
   * @param req 
   * @param res 
   * @param next 
   */
  public async getBook(req, res, next){
    try {
      const { id } = req.headers;
      const result = await BookData.model.findOne({_id: id});
      const rdata = { req, res, status: 200, data: result };
      Helpers.transformResponse(rdata);
    } catch (e) {
      next(e);
    }
  }

  /**
   * update()
   * Method is used to update a specific book
   * @author Abhijeet
   * @param req 
   * @param res 
   * @param next 
   */
  public async update(req, res, next) {
    try {
      const { id } = req.params;
      const { author, title, isbn, release_date } = req.body;
      let data = {
        id,
        author,
        title,
        isbn,
        release_date
      };
      
      const result = await BookData.model.findOneAndUpdate({_id: id}, data, {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
      });
      const rdata = { req, res, status: 200, data: result };
      Helpers.transformResponse(rdata);
        
    } catch (e) {
      next(e);
    }
  }

  /**
   * getAll()
   * get all book
   * @author Abhijeet Ranjan
   * @param req
   * @param res
   * @param next
   */

  public async getAll(req, res, next) {
    try {
      let result = await BookData.model.find();
      const rdata = { req, res, status: 200, data: result };
      Helpers.transformResponse(rdata);
    } catch (e) {
      next(e);
    }
  }
}

export default new BookController();

