import { Server } from './server';
process.on('uncaughtException', e => {
  console.log(e);
  process.exit(1);
});

process.on('unhandledRejection', e => {
  console.log(e);
  process.exit(1);
});
let server = new Server().app;
let port = process.env.PORT || 5000;

/**
 * start server
 * @developer: Abhijeet
 */
server.listen(port, () => {
  console.log(`server is running on port ${port}`);
});
