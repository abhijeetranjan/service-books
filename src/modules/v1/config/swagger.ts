const swaggerJsdoc = require('swagger-jsdoc');
const options = {
    swaggerDefinition: {
      info: {
        title: 'Orchestrator',
        version: '2.0.0',
        description: 'All v1 routes documentation',
      },
      // host: `${process.env.INTERNAL_API}`,
      basePath: '/v1', 
    },
    // List of files to be processes. You can also set globs './routes/*.js'
    apis: ['./src/modules/v1/routers/*.ts'],
  };

  export default swaggerJsdoc(options);