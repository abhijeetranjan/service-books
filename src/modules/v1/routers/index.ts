import { Router } from 'express';
import BookRouter from './BookRouter';
const swaggerUi = require('swagger-ui-express');
import swaggerSpecs from '../config/swagger';

const routes = Router();

routes.use('/book', BookRouter);
/**
 * @swagger
 * /book:
 *    get:
 *      description: This should return all books
 *      responses:
 *        "200":
 *          description: All books
 *          content:
 *            application/json
 *
 */

/**
 * @swagger
 * /book/details:
 *    get:
 *      description: This should return specific book
 *      parameters:
 *        - in: header
 *          name: id
 *          description: The id of book.
 *          required: true
 *      responses:
 *        "200":
 *          description: Get specific book
 *          content:
 *            application/json
 *      
 */

 /**
 * @swagger
 * /book:
 *   post:
 *    summary: Creates a new book.
 *    consumes:
 *      - application/json
 *    parameters:
 *      - in: body
 *        name: book
 *        description: The book to create.
 *        schema:
 *          type: object
 *          required:
 *             - title
 *             - author
 *             - isbn
 *             - release_date
 *          properties:
 *             title:
 *               type: string
 *             author:
 *               type: string
 *             isbn:
 *               type: string
 *             release_date:
 *               type: string
 *    responses:
 *       200:
 *         description: Success
 */

 /**
 * @swagger
 * /book/{id}:
 *   put:
 *    summary: Update a new book.
 *    consumes:
 *      - application/json
 *    parameters:
 *      - in: path
 *        name: id
 *        description: The id of book.
 *        required: true
 *      - in: body
 *        name: book
 *        description: The book to update.
 *        schema:
 *          type: object
 *          required:
 *             - title
 *             - author
 *             - isbn
 *             - release_date
 *          properties:
 *             title:
 *               type: string
 *             author:
 *               type: string
 *             isbn:
 *               type: string
 *             release_date:
 *               type: string
 *    responses:
 *       200:
 *         description: Success
 */
routes.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));

export default routes;
