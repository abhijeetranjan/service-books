import { Router } from 'express';
import BookController from '../controllers/BookController';
import BookValidators from '../validators/BookValidators';
import GlobalMiddleWare from '../middlewares/GlobalMiddleWare';

class BookRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.getAllRoutes();
    this.getRoutes();
    this.putRoutes();
    this.saveRoutes();
  }

  private getAllRoutes() {
    this.router.get(
      '/',
      GlobalMiddleWare.checkError,
      BookController.getAll
    );
  }

  private getRoutes() {
    this.router.get(
      '/details',
      BookValidators.getBook(),
      GlobalMiddleWare.checkError,
      BookController.getBook
    );
  }

  private saveRoutes() {
    this.router.post(
      '/',
      BookValidators.saveBook(),
      GlobalMiddleWare.checkError,
      BookController.save
    );
  }

  private putRoutes() {
    this.router.put(
      '/:id',
      BookValidators.updateBook(),
      GlobalMiddleWare.checkError,
      BookController.update
    );
  }
}

export default new BookRouter().router;
