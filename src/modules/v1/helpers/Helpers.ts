import { Request, Response } from 'express';
import * as _ from 'lodash';

type IresponseData = {
  req: Request;
  res: Response;
  status: number;
  data?: any;
  errors?: any;
};

/**
 * @class Helpers
 */
class Helpers {
  /**
   * ? Access Modifier : public
   * * transform Response
   *
   * @author Abhijeet Ranjan
   *
   * @param responseData
   */

  public async transformResponse(responseData: IresponseData) {
    let { data, status, errors, res, req } = responseData;

    res.status(status).json({
      status,
      data,
      errors
    });
  }

}

//! create object of helper class
export default new Helpers();
