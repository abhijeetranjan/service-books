
class TimeOptions {
  constructor() {}

  protected setTimestampConfig() {
    const options: any = {
      versionKey: false,
      timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        currentTime: () => this.getDateUnixTimestamp()
      }
    };
    return options;
  }

  public getDateUnixTimestamp() {
    return Math.floor(Date.now() / 1000);
  }

  public convertTimestampToIsoDate(timestamp: any) {
    return new Date(timestamp * 1000).toISOString();
  }
}

export default TimeOptions;
