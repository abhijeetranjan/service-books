import { Schema, model, Document, Model } from 'mongoose';
import TimeOptions from './timeOptions.config';
import IBook from '../interfaces/book.interface';


class BookData extends TimeOptions {
  private _model: Model<IBook>;

  constructor() {
    super();
    const schema = new Schema(
      {
        author: {
          type: String,
          required: true
        },
        title: {
          type: String,
          unique: true,
          required: true
        },
        isbn: {
          type: String,
          required: true
        },
        release_date:{
          type: Date,
          required: true
        },
        created_at: { type: Schema.Types.Mixed },
        updated_at: { type: Schema.Types.Mixed }
      },
      this.setTimestampConfig()
    );

    schema.methods = {
      toJSON() {
        let result: any = {
          id: this._id,
          author: this.author,
          title: this.title,
          isbn: this.isbn,
          release_date: this.release_date,
          created_at: new TimeOptions().convertTimestampToIsoDate(this.created_at),
          updated_at: new TimeOptions().convertTimestampToIsoDate(this.updated_at)
        };
        return result
      }
    };

    this._model = model<IBook>('BookData', schema);
  }

  public get model(): Model<IBook> {
    return this._model;
  }
}
export default new BookData();