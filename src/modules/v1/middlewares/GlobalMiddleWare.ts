import { validationResult } from 'express-validator';
export default new (class GlobalMiddleWare {
  /**
   * checkError
   * retrieve validation errors and pass all validations array to express next middleware function
   *
   * @author Abhijeet
   *
   * @param req
   * @param res
   * @param next
   */

  public checkError(req, res, next) {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      next(new Error(error.array()[0].msg));
    } else {
      next();
    }
  }
})();
