import Common from './common';
import { Router } from 'express';
type Wrapper = (router: Router) => void;
class Middlewares {
  public apply(router: Router) {
    const middlewareWrappers: Wrapper[] = [
      Common.handleCors,
      Common.handleBodyRequestParsing
    ];
    for (const wrapper of middlewareWrappers) {
      wrapper(router);
    }
  }
}

export default new Middlewares();
