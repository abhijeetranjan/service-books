import { Router } from 'express';
import * as cors from 'cors';
import * as parser from 'body-parser';

class Common {
  public handleCors(router: Router) {
    if(process.env.ACCESSDOMAIN) {
      let accessDomain = process.env.ACCESSDOMAIN;
      // replace %2f with / in accessdomain
      if (accessDomain.includes('%2f')) {
        accessDomain = accessDomain.replace(/%2f/g, '/');
      }
      const corsOptions = {
        origin: accessDomain.includes(',') ? accessDomain.split(',') : accessDomain,
        methods: ['GET', 'POST', 'DELETE', 'OPTIONS', 'PATCH', 'PUT']
      };
      router.use(cors(corsOptions));

      router.use((req: any, res, next) => {
        // remove header key X-Powered-By from request
        res.removeHeader('X-Powered-By');
        // This code is to deny request if hit by postman or from origin that not allowed in
        let headerOrigin:any = req.headers.origin;
        if (accessDomain != '*' && typeof req.query.internal === 'undefined' && !accessDomain.includes(headerOrigin) ) {
          // security checks conditions
          req.errorStatus = 403;
          next(new Error('Request denied by host due to unauthorized access domain.'));
        }
        next();
      });
    }
  }

  public handleBodyRequestParsing(router: Router) {
    router.use(parser.urlencoded({ limit: '50mb', extended: true }));
    router.use(parser.json({ limit: '50mb' }));
  }
}

export default new Common();
