import * as mongoose from 'mongoose';

class Utils {
  mongoId(id) {
    return mongoose.Types.ObjectId(id);
  }

  isValidMongoId(id) {
    return mongoose.Types.ObjectId.isValid(id);
  }

  generateUrl(url: string) {
    const envParam = url.substring(0, url.indexOf('/'));
    url = url.replace(envParam, process.env[envParam]);
    return url;
  }

  isJson(str) {
    try {
      return JSON.parse(str);
    } catch (e) {
      return str;
    }
  }
}

export default new Utils();
