import * as express from 'express';
import * as mongoose from 'mongoose';
import constants from '../../config/constants';
import routes from './routers';
import Middlewares from './middlewares';
import Helpers from './helpers/Helpers';
import Utils from './utils/Utils';
import { Messages } from '../../lang/en/messages';

export class Server {
  //* express application object
  public app: express.Application = express();

  constructor() {
    this.connectMongoDb();
    this.configMiddleware();
    this.setRoutes();
    this.error404Handler();
    this.handleErrors();
  }

  configMiddleware() {
    Middlewares.apply(this.app);
  }

  /**
   * connectMongoDb
   * database connection established
   * @author: Abhijeet
   * Todo: pass error in next function and log error
   */

  async connectMongoDb() {
    mongoose.set('useCreateIndex', true);
    mongoose.set('useFindAndModify', false);
    await mongoose
      .connect(constants.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
      .then(() => {
        console.log(Messages.mongo.connection_success);
      })
      .catch(error => {
        // pass to next function and log error /** todo */
        console.log(Messages.mongo.connection_failure, error.message);
      });
  }

  /**
   * Set all routes
   */
  setRoutes() {
    this.app.use('/v1', routes);
  }

  /**
   * handle 404 error if route does not match with defined
   */
  error404Handler() {
    this.app.use((req, res) => {
      const errorStatus = 404;
      const message = Messages.resource_not_found.message;
      return Helpers.transformResponse({ req, res, status: errorStatus, errors: message });
    });
  }

  /**
   * This is global function which will handle all occured errors
   */
  handleErrors() {
    this.app.use((error, req, res, next) => {
      const errorStatus = req.errorStatus || 500;
      const message = Utils.isJson(error.message) || Messages.something_went_wrong.message;
      return Helpers.transformResponse({ req, res, status: errorStatus, errors: message });
    });
  }
}
