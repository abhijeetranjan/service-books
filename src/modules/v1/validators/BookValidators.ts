import { header, body, param } from 'express-validator';
export default new (class BookValidators {
  constructor() {}

  public saveBook() {
    return [
      body('author','author does not exist').exists().isString(),
      body('title','title does not exist').exists().isString(),
      body('isbn','isbn does not exist').exists().isString(),
      body('release_date','release_date does not exist').exists().isString()
    ];
  }
  
  public updateBook() {
    return [
      param('id', 'id does not exist')
        .exists()
        .isString()
    ];
  }

  public getBook() {
    return [
      header('id', 'id does not exist')
        .exists()
        .isString()
    ];
  }
})();
