import { Document } from 'mongoose';

export default interface IBook extends Document {
    author: string;
    title: string;
    isbn: string;
    release_date: string;
}