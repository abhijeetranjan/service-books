export interface requestParams {
  body: object;
  headers: object;
  query: object;
  path: object;
}
