/* eslint-disable no-console */
import express from 'express';
import dotenv from 'dotenv';
import constants from './config/constants';
import connectDb from './config/database';
import middlewaresConfig from './config/middlewares';
import apiRoutes from './modules';
import { Messages } from './src/lang/en/messages';
import * as helper from './helpers/helper';

const app = express();

middlewaresConfig(app);

// initialise environment
dotenv.config();

// morgan(app);
apiRoutes(app);

process.on('uncaughtException', error => {
  /** Will handle uncaught exceptions  */
  helper.fileErrorLog(error.message || error);
  process.exit(1);
});

connectDb()
  .then(async () => {
    const server = await app.listen(constants.PORT, err => {
      if (err) {
        console.log(Messages.server.connection_failure, err.message);
        helper.fileErrorLog(err.message || err);
        process.exit(0);
      }
      console.log(Messages.server.connection_success,` on port ${constants.PORT}!`);
    });
    
  })
  .catch(error => {
    console.log(Messages.mongo.connection_failure, error.message);
    helper.fileErrorLog(error.message || error);
  });

module.exports = app;
