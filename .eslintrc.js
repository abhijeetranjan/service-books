const fs = require('fs');

const prettierOptions = JSON.parse(fs.readFileSync('./.prettierrc', 'utf8'));

// http://eslint.org/docs/user-guide/configuring
// https://github.com/prettier/prettier#eslint
module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb-base', 'prettier'],
  plugins: ['flowtype', 'prettier'],
  env: {
    jest: true
  },
  rules: {
    'prettier/prettier': ['error', prettierOptions],
    'flowtype/define-flow-type': 1,
    'no-case-declarations': 0,
    'no-underscore-dangle': 0,
    'consistent-return': 0,
    'no-restricted-syntax': 0,
    eqeqeq: 0,
    'no-unreachable': 1,
    'no-use-before-define': 1,
    'no-await-in-loop': 0,
    'no-param-reassign': 0,
    'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
    'no-loop-func': 0,
    'no-shadow': 0,
    'import/no-dynamic-require': 0,
    'global-require': 0,
    'no-return-await': 0,
    'no-plusplus': 1,
    'block-scoped-var': 1,
    'vars-on-top': 1,
    'no-redeclare': 1,
    'no-var': 1,
    'no-undef': 1,
    'import/no-named-as-default': 1,
    'import/no-named-as-default-member': 1,
    'no-continue': 0,
    'no-unused-expressions': 0,
    'no-new-object': 1,
    'import/prefer-default-export': 1,
    'no-nested-ternary': 0,
    'array-callback-return': 0,
    'class-methods-use-this': 0,
    'no-return-assign': 0,
    'new-cap': 1,
    'prefer-const': 1,
    'one-var': 1,
    'default-case': 1,
    'no-console': 0,
    'guard-for-in': 0,
    'import/no-duplicates': 1,
    'func-names': 1,
    'linebreak-style': 0,
    'prefer-destructuring': 1,
    'import/named': 0,
    'import/no-cycle': 0
  }
};
