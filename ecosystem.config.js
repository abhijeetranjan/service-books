require('dotenv').config();

module.exports = {
  apps: [
    {
      name: 'orchestrator',
      script: process.env.REGION ? 'build/modules/v2/index.js' : 'dist/index.js',
      watch: false,
      instances: 1,
      cwd: './',
      max_restarts: 5,
      exec_mode: 'cluster',
      max_memory_restart: '200M',
      env: {
        PORT: 3007,
        NODE_ENV: 'production'
      }
    }
  ]
};
